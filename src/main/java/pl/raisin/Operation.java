package pl.raisin;

public enum Operation {
    PLUS,
    MINUS,
    MULTIPLY,
    DIVIDE
}
