package pl.raisin;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

import javafx.event.ActionEvent;

public class CalcController {

    @FXML
    Button buttonAC;
    @FXML
    Button buttonC;
    @FXML
    Button buttonSeven;
    @FXML
    Button buttonEight;
    @FXML
    Button buttonPlus;
    @FXML
    Button buttonFour;
    @FXML
    Button buttonTwo;
    @FXML
    Button buttonSix;
    @FXML
    Button buttonMinus;
    @FXML
    Button buttonOne;
    @FXML
    Button buttonFive;
    @FXML
    Button buttonThree;
    @FXML
    Button buttonMultiply;
    @FXML
    Button buttonEquals;
    @FXML
    Button buttonDivide;
    @FXML
    Button buttonNine;
    @FXML
    Button buttonZero;
    @FXML
    Button buttonDot;
    @FXML
    TextField display;

    private Operation currentOperation;
    private double previousNumber = 0;

    private void setOperation(Operation operation ) {
        currentOperation = operation;
        previousNumber = getNumber();
        display.clear();
        // TODO:
        System.out.println(operation);
    }

    private double getNumber() {
        String text = display.getText();
        try {
            return Double.parseDouble(text.replace(",", "."));
        } catch (Exception e) {
            return 0;
        }
    }

    @FXML
    public void onClickNumber(ActionEvent event) {
        System.out.println("Number " + event);
        Object source = event.getSource();
        if (source instanceof Button) {
//        if (source.getClass().equals(Button.class)) {
            Button button = (Button) source;
            String text = button.getText();
            display.setText(text);
        }
    }
    @FXML
    public void onClickC() {
        display.setText("");
        System.out.println("Clear");
    }
    @FXML
    public void onClickAC() {
        display.setText("");
        System.out.println("All clear");
    }
    @FXML
    public void onClickPlus() {
        setOperation(Operation.PLUS);
        System.out.println("Plus");
    }
    @FXML
    public void onClickMinus() {
        setOperation(Operation.MINUS);
        System.out.println("Minus");
    }
    @FXML
    public void onClickDivide() {
        setOperation(Operation.DIVIDE);
        System.out.println("Divide");
    }
    @FXML
    public void onClickMultiply() {
        setOperation(Operation.MULTIPLY);
        System.out.println("Multipy");
    }
    @FXML
    public void onClickDot() {
        // TODO:
        System.out.println("Dot");
    }
    @FXML
    public void onClickEquals() {
        double b = getNumber();
        double c = getResult(previousNumber, b);
        display.setText(String.format("%f", c));
    }

    private double getResult(double a, double b) {
        switch (currentOperation) {
            case DIVIDE:
                // TODO: divide by zero ?
                return a / b;
            case MULTIPLY:
                return a * b;
            case PLUS:
                return a + b;
            case MINUS:
                return a - b;
            default:
                // TODO:
                return 0;
        }
    }


}
